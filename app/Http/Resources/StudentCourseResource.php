<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentCourseResource extends JsonResource
{

     /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'student_name' => $this->student->name ?? null,
            'student_email' => $this->student->email ?? null,
            'course_code' => $this->course->code ?? null,
            'course_name' => $this->course->name ?? null,
            'course_description' => $this->course->description ?? null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
    

}
