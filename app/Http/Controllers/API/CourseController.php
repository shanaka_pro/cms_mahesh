<?php

namespace App\Http\Controllers\API;

use App\Models\Course;
use App\Http\Resources\CourseResource;

class CourseController extends BaseController
{

    /**
     * @OA\Get(
     *      path="/api/courses",
     *      operationId="getBuyersList",
     *      tags={"Courses"},
     *      summary="Get list of courses",
     *      description="Returns list of courses",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     ),
     *)
     */

    public function index()
    {
        return $this->sendResponse(CourseResource::collection(Course::all()), 'Records retrieved successfully.');
    }
}
