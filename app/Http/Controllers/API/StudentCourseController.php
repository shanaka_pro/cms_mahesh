<?php

namespace App\Http\Controllers\API;

use App\Models\StudentCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\StudentCourseResource;


class StudentCourseController extends BaseController
{

    /**
     * @OA\Get(
     *      path="/api/student-courses",
     *      operationId="getStudentCourseList",
     *      tags={"Student Course"},
     *      summary="Get list of courses",
     *      description="Returns list of courses",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     ),
     *)
     */

    public function index()
    {

        return $this->sendResponse(StudentCourseResource::collection(StudentCourse::all()), 'Records retrieved successfully.');
    }


    /**
     * @OA\Post(
     *      path="/api/student-courses",
     *      operationId="storeStudentCourse",
     *      tags={"Student Course"},
     *      summary="Store new student course",
     *      description="Returns student course data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StudentCourse")
     *      ),
     *  security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StudentCourse")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [

            'student_id' => 'numeric|required',
            'course_id' => 'numeric|required',

        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $studentCourse = StudentCourse::create($data);

        return $this->sendResponse($studentCourse, 'Record Created successfully.');
    }

    /**
     * @OA\Get(
     *      path="/api/student-courses/{id}",
     *      operationId="getBuyerById",
     *      tags={"Student Course"},
     *      summary="Get student course information",
     *      description="Returns student course data",
     *      @OA\Parameter(
     *          name="id",
     *          description="StudentCourse id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */

    public function show($id)
    {
        $studentCourse = StudentCourse::find($id);

        if (is_null($studentCourse)) {
            return $this->sendError('Record not found.');
        }

        return $this->sendResponse(new StudentCourseResource($studentCourse), 'Record retrieved successfully.');
    }


    /**
     * @OA\Get(
     *      path="/api/student-courses-show",
     *      operationId="getStudentCourseShow",
     *      tags={"Student Course"},
     *      summary="Get list of student`s courses",
     *      description="Returns list of student`s courses",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     ),
     *)
     */

    public function showCourses()
    {

        $studentCourse = StudentCourse::where('student_id', \Auth::user()->id)->get();

        return $this->sendResponse(StudentCourseResource::collection($studentCourse), 'Records retrieved successfully.');
    }

    /**
     * @OA\Put(
     *      path="/api/student-courses/{id}",
     *      operationId="updateStudent Course",
     *      tags={"Student Course"},
     *      summary="Update existing student course",
     *      description="Returns updated student course data",
     *      @OA\Parameter(
     *          name="id",
     *          description="StudentCourse_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  security={ {"bearer_token": {}}},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StudentCourse")
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StudentCourse")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */

    public function update(Request $request, StudentCourse $studentCourse)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'student_id' => 'numeric|required',
            'course_id' => 'numeric|required',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
        }

        $status = $studentCourse->update($request->all());

        return $this->sendResponse($status, 'Record Updated successfully.');
    }

    /**
     * @OA\Delete(
     *      path="/api/student-courses/{id}",
     *      operationId="deleteStudentCourse",
     *      tags={"Student Course"},
     *      summary="Delete existing studentCourse",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="StudentCourse id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *  security={ {"bearer_token": {}}},
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy($id)
    {

        $studentCourse = StudentCourse::find($id);

        if (is_null($studentCourse)) {
            return $this->sendError('Record not found.');
        }

        $studentCourse->delete();

        return $this->sendResponse([], 'Record deleted successfully.');
    }
}
