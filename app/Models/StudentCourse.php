<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *     title="Student Course",
 *     description="Student Course model",
 *     @OA\Xml(
 *         name="Student Course"
 *     )
 * )
 */

class StudentCourse extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'student_id',
        'course_id'
    ];


    /**
     * Get the student that owns the StudentCourse.
     */
    public function student()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the course that owns the StudentCourse.
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }



    /**
     * @OA\Property(
     *      title="student_id",
     *      description="student_id of the new StudentCourse",
     *      format="int64",
     *      example="1"
     * )
     *
     * @var string
     */
    private $student_id;


    /**
     * @OA\Property(
     *      title="course_id",
     *      description="course_id of the new StudentCourse",
     *      format="int64",
     *      example="1"
     * )
     *
     * @var string
     */
    private $course_id;
}
